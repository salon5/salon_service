run:
	go run cmd/app/main.go

proto-gen:
	./internal/script/gen-proto.sh

update_submodule:
	git submodule update --remote --merge

create_proto_submodule:
	git submodule add git@gitlab.com:salon5/protos.git

migrate_up:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/salon_evron up

migrate_down:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/salon_evron down

migrate_force:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/salon_evron force 2
	

 
  