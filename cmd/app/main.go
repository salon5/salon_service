package main

import (
	"gitlab.com/salon5/salon_service/config"
	"gitlab.com/salon5/salon_service/internal/app"
)

func main() {
	cfg := config.Load()

	app.Run(cfg)
}