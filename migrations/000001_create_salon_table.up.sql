CREATE TABLE IF NOT EXISTS salon(
    id SERIAL PRIMARY KEY,
    salon_name TEXT NOT NULL,
    description TEXT NOT NULL,
    auto_id INT,
    customer_id INT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
);