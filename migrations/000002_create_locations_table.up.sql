CREATE TABLE IF NOT EXISTS locations(
    id SERIAL PRIMARY KEY,
    salon_id INT NOT NULL REFERENCES salon(id),
    country TEXT NOT NULL,
    street TEXT NOT NULL,
    district TEXT NOT NULL,
    phonenumber TEXT NOT NULL
);