FROM golang:1.19.3-alpine
RUN mkdir salon
COPY . /salon
WORKDIR /salon
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 4444
