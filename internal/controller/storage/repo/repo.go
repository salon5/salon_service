package repo

import ss "gitlab.com/salon5/salon_service/internal/genproto/salon"

type SalonStorageI interface {
	CreateSalon(*ss.CreateSalonReq) (*ss.CreateSalonResp, error) 
	GetSalon(*ss.GetSalonReq) (*ss.GetSalonResp, error)

	CreateLocation(*ss.Location) (*ss.Location, error)
	GetLocations(*ss.GetSalonReq) ([]*ss.Location, error)

	CheckField(*ss.CheckFieldReq) (*ss.CheckFieldResp, error)
	GetSalonBySalonName(*ss.GetSalonBySalonNameReq) (*ss.CreateSalonResp, error)
	UpdateFieldById(*ss.UpdateFieldByIdReq) (*ss.Empty, error)
	

}