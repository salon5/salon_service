package storage

import (
	"gitlab.com/salon5/salon_service/internal/controller/storage/postgres"
	"gitlab.com/salon5/salon_service/internal/controller/storage/repo"
	"gitlab.com/salon5/salon_service/pkg/db"
)

type IStorage interface {
	Salon() repo.SalonStorageI
}

type StoragePg struct {
	Db        *db.Postgres
	SalonRepo repo.SalonStorageI
}

func NewStoragePg(db *db.Postgres) *StoragePg {
	return &StoragePg{
		Db:        db,
		SalonRepo: postgres.NewSalonRepo(db),
	}
}

func (s StoragePg) Salon() repo.SalonStorageI {
	return s.SalonRepo
}
