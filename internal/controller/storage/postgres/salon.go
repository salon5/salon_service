package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	ss "gitlab.com/salon5/salon_service/internal/genproto/salon"
)


func (s *SalonRepo) CreateSalon(req *ss.CreateSalonReq) (*ss.CreateSalonResp, error) {
	responce := &ss.CreateSalonResp{}
	create_time := time.Time{}
	updated_time := time.Time{}
	err := s.Db.Pool.QueryRow(context.Background(),`
	INSERT INTO 
		salon(
			salon_name, 
			description, 
			auto_id, 
			customer_id, 
			password, 
			refresh_token
		)
	VALUES($1, $2, $3, $4, $5, $6)
	returning 
		id, 
		salon_name, 
		description, 
		auto_id, 
		customer_id, 
		password, 
		refresh_token, 
		created_at, 
		updated_at`,
	req.SalonName,
	req.Description,
	req.AutoId,
	req.CustomerId,
	req.Password,
	req.RefreshToken).Scan(
		&responce.Id,
		&responce.SalonName,
		&responce.Description,
		&responce.AutoId,
		&responce.CustomerId,
		&responce.Password,
		&responce.RefreshToken,
		&create_time,
		&updated_time,
	)
	if err != nil {
		fmt.Println(`ERror`, err)
		return &ss.CreateSalonResp{}, err
	}

	for _, insert := range req.Locations {
		insert.SalonId = responce.Id
		_ ,err := s.CreateLocation(insert)
		if err != nil {
			return &ss.CreateSalonResp{}, err
		}
		responce.Locations = append(responce.Locations, insert)
	}
	responce.CreatedAt = create_time.String()
	responce.UpdatedAt = updated_time.String()

	return responce, nil
}

func (s *SalonRepo) CreateLocation(req *ss.Location) (*ss.Location, error) {
	res := &ss.Location{}
	err := s.Db.Pool.QueryRow(context.Background(), `
	INSERT INTO 
		locations(salon_id, country, street, district, phonenumber)
	VALUES($1, $2, $3, $4, $5) returning id, salon_id, country, street, district, phonenumber`,
	req.SalonId,
	req.Country, 
	req.Street, 
	req.District, 
	req.PhoneNumber).Scan(
		&res.LocationId,
		&res.SalonId,
		&res.Country,
		&res.Street,
		&res.District,
		&res.PhoneNumber,
	)
	if err != nil {
		return &ss.Location{}, err
	}

return res, nil
}

func (s *SalonRepo) GetSalon(req *ss.GetSalonReq) (*ss.GetSalonResp, error) {
	responce := &ss.GetSalonResp{}
	fmt.Println(req)
	err := s.Db.Pool.QueryRow(context.Background(),`
	SELECT id, salon_name, description, auto_id, customer_id FROM salon WHERE id=$1`,req.SalonId).Scan(
		&responce.Id,
		&responce.SalonName,
		&responce.Description,
		&responce.AutoId,
		&responce.CustomerId,
	)
	if err != nil {
		fmt.Println(err)
		return &ss.GetSalonResp{}, err
	}
	
	responce.Locations, err = s.GetLocations(req)
	if err != nil {
		return &ss.GetSalonResp{}, err
	}

return responce, nil	
}
	
func (s *SalonRepo) GetLocations(req *ss.GetSalonReq) ([]*ss.Location, error) {
	res := []*ss.Location{}

	rows, err := s.Db.Pool.Query(context.Background(),`
	SELECT id, salon_id, country, street, district, phonenumber FROM locations WHERE salon_id=$1`, req.SalonId)
	if err != nil {
		
		return []*ss.Location{}, err
	}
	defer rows.Close()
	for rows.Next() {
		address := &ss.Location{}
		err := rows.Scan(
			&address.LocationId,
			&address.SalonId,
			&address.Country,
			&address.Street,
			&address.District,
			&address.PhoneNumber,
		)
		if err != nil {
			return  []*ss.Location{}, err
		}
		res = append(res, address)
	}

	return res, nil
}

func (s *SalonRepo) CheckField (req *ss.CheckFieldReq) (*ss.CheckFieldResp, error) {
	responce := &ss.CheckFieldResp{Exists: false}
	query := fmt.Sprintf("SELECT 1 FROM salon WHERE %s=$1", req.Field)
	var temp = 0
	err := s.Db.Pool.QueryRow(context.Background(), query, req.Value).Scan(&temp)
	if err == sql.ErrNoRows {
		return responce, err 
	}else if err != nil {
		return responce, err
	}
	if temp == 1{
		responce.Exists = true
		return responce, err
	}
	return responce, nil
}

func (s *SalonRepo) GetSalonBySalonName(req *ss.GetSalonBySalonNameReq) (*ss.CreateSalonResp, error) {
	
	responce := &ss.CreateSalonResp{}
	var createTime, updateTime time.Time
	err := s.Db.Pool.QueryRow(context.Background(), `
	SELECT 
		id,
		salon_name, 
		description, 
		auto_id, 
		customer_id, 
		password,
		refresh_token, 
		created_at, 
		updated_at 
	FROM salon WHERE salon_name=$1`, req.SalonName).Scan(
		&responce.Id,
		&responce.SalonName,
		&responce.Description,
		&responce.AutoId,
		&responce.CustomerId,
		&responce.Password,
		&responce.RefreshToken,
		&createTime,
		&updateTime,
	)
	if err != nil {
		return &ss.CreateSalonResp{}, err
	}
	responce.CreatedAt = createTime.String()
	responce.UpdatedAt = updateTime.String()
	
	return responce, nil

}

func (s *SalonRepo) UpdateFieldById(req *ss.UpdateFieldByIdReq) (*ss.Empty, error) {
	res := &ss.Empty{}
	query := fmt.Sprintf("UPDATE salon SET %s='%s' WHERE id=$1", req.Field, req.Value)
	err := s.Db.Pool.QueryRow(context.Background(),query, req.Id).Scan(&res)
	if err == sql.ErrNoRows {
		return &ss.Empty{}, err
	}
	return res, nil
}
