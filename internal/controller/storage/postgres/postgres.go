package postgres

import "gitlab.com/salon5/salon_service/pkg/db"



type SalonRepo struct {
	Db *db.Postgres
}

func NewSalonRepo(pdb *db.Postgres) *SalonRepo {
	return &SalonRepo{Db: pdb}
} 