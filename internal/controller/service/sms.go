package service

import (
	"context"

	"gitlab.com/salon5/salon_service/internal/genproto/salon"
	"gitlab.com/salon5/salon_service/internal/message_broker/models"
)

func (s *SalonService) SendSmSVerification(ctx context.Context, req *salon.SendSmSVerificationReq) (*salon.Empty, error) {
	err := s.kafkaProducer.SendSmSVerfication(&models.SendSMSVerificationCodeReq{
		Code: req.Code,
		To:   req.To,
	})
	if err != nil {
		s.Logger.Error(err)
		return &salon.Empty{}, err
	}

	return &salon.Empty{}, nil

}
