package service

import (
	grpcclient "gitlab.com/salon5/salon_service/internal/controller/service/grpcClient"
	"gitlab.com/salon5/salon_service/internal/controller/storage"
	messagebroker "gitlab.com/salon5/salon_service/internal/message_broker"
	"gitlab.com/salon5/salon_service/pkg/db"
	"gitlab.com/salon5/salon_service/pkg/logger"
)

type SalonService struct {
	Logger        *logger.Logger
	Client        grpcclient.Clients
	Storage       storage.IStorage
	kafkaProducer *messagebroker.Producer
}

func NewSalonService(l *logger.Logger, client grpcclient.Clients, DB *db.Postgres, producer *messagebroker.Producer) *SalonService {
	return &SalonService{
		Logger:        l,
		Client:        client,
		Storage:       storage.NewStoragePg(DB),
		kafkaProducer: producer,
	}
}
