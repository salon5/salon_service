package service

import (
	"context"
	"fmt"

	"gitlab.com/salon5/salon_service/internal/genproto/autoinfo"
	cc "gitlab.com/salon5/salon_service/internal/genproto/customer"
	ss "gitlab.com/salon5/salon_service/internal/genproto/salon"
	"gitlab.com/salon5/salon_service/pkg/logger"
)

func(s *SalonService) UpdateFieldById(ctx context.Context, req *ss.UpdateFieldByIdReq) (*ss.Empty, error) {
	field, err := s.Storage.Salon().UpdateFieldById(req)
	if err != nil {
		s.Logger.Error(err)
		return &ss.Empty{}, err
	}
	return field, nil
}

func (s *SalonService) GetSalonBySalonName(ctx context.Context, req *ss.GetSalonBySalonNameReq) (*ss.CreateSalonResp, error) {
	responce, err := s.Storage.Salon().GetSalonBySalonName(req)
	if err != nil {
		s.Logger.Error(err)
		return &ss.CreateSalonResp{}, err
	}
	return responce, nil
}

func (s *SalonService) CheckField(ctx context.Context, req *ss.CheckFieldReq) (*ss.CheckFieldResp, error) {
	responce, err := s.Storage.Salon().CheckField(req)
	if err != nil {
		s.Logger.Error(err)
		return &ss.CheckFieldResp{}, err
	}
	return responce, nil
}
 
func (s *SalonService) CreateSalon(ctx context.Context, req  *ss.CreateSalonReq) (*ss.CreateSalonResp, error) {
	res, err := s.Storage.Salon().CreateSalon(req)
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while inserting SALON`, logger.New(`Error with SALON`))
		return &ss.CreateSalonResp{}, err
	}
	return res, nil
}

func (s *SalonService) GetSalon(ctx context.Context, req *ss.GetSalonReq) (*ss.GetSalonResp, error) {
	res, err := s.Storage.Salon().GetSalon(req)
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting SALON`, logger.New(err.Error()))
		return &ss.GetSalonResp{}, err
	}
	
	return res, nil
}

func (s *SalonService)	GetSalonWithCustomer(ctx context.Context, req *ss.GetSalonReq) (*ss.GetSalonWithCustomerResp, error) {
	responce, err := s.Storage.Salon().GetSalon(req)
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting SALON`, logger.New(err.Error()))
		return &ss.GetSalonWithCustomerResp{}, err
	}
	
	// res := &ss.GetSalonWithCustomerResp{}
	res := (*ss.GetSalonWithCustomerResp)(responce)
	customer, err := s.Client.Customer().GetCustomers(ctx, &cc.CustomerID{CustomerId: responce.CustomerId})
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting Customer`, logger.New(err.Error()))
		return nil, err
	}
	cus := &ss.GetCustomerResp{
		Id: res.CustomerId,
		SalonId: customer.SalonId,
		AutoId: customer.AutoId,
		Fullname: customer.Fullname,
		Email: customer.Email,
		Phonenumber: customer.Phonenumber,
	}
	for _, address := range customer.Addresses {
		fmt.Println(`Hello`)
		temp := &ss.Address{
			Id: address.Id,
			CustomerId: cus.Id,
			District: address.District,
			Street: address.Street,
			Homenumber: address.Homenumber,
		}
		cus.Addresses = append(cus.Addresses, temp)
	}
	fmt.Println(cus.Addresses)
	res.GetCustomer = append(res.GetCustomer, (*ss.GetCustomerResp)(cus))
	
	return res, nil	
}

func (s *SalonService) GetSalonWithAuto(ctx context.Context, req *ss.GetSalonReq) (*ss.GetSalonWithAutoResp, error) {
	responce, err := s.Storage.Salon().GetSalon(req)
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting SALON`, logger.New(err.Error()))
		return &ss.GetSalonWithAutoResp{}, err
	}
	res := &ss.GetSalonWithAutoResp{
		Id: responce.Id,
		CustomerId: responce.CustomerId,
		AutoId: responce.AutoId,
		SalonName: responce.SalonName,
		Description: responce.Description,
	}
	for _, res_in := range responce.Locations{
		temp := &ss.Location{
			LocationId: res_in.LocationId,
			SalonId: res.Id,
			Country: res_in.Country,
			Street: res_in.Street,
			District: res_in.District,
			PhoneNumber: res_in.PhoneNumber,
		}
		res.Locations = append(res.Locations, temp)
	}
	auto, err := s.Client.AutoInfo().GetAuto(ctx, &autoinfo.GetAutoReq{AutoId: res.AutoId})
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting AUTO INFO`, logger.New(err.Error()))
		return nil, err
	}
	auto_info := &ss.GetAutoResp{
		Id: auto.Id,
		AutoName: auto.AutoName,
		Model: auto.Model,
		SalonId: auto.SalonId,
		CustomerId: auto.CustomerId,
		Price: auto.Price,
		Country: auto.Country,
	}
	for _, auto_in := range auto.Types {
		plus := &ss.AutoType{
			Id: auto_in.Id,
			AutoId: auto_info.Id,
			Auto_Type: auto_in.Auto_Type,
			Marka: auto_in.Marka,
		}
		auto_info.Types = append(auto_info.Types, plus)
	}
	res.GetAuto = append(res.GetAuto, (*ss.GetAutoResp)(auto_info))

	return res, nil
}

func (s *SalonService) GetSalonAllInfo(ctx context.Context, req *ss.GetSalonReq) (*ss.GetSalonAllInfoResp, error) {
	responce, err := s.Storage.Salon().GetSalon(req)
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting SALON`, logger.New(err.Error()))
		return &ss.GetSalonAllInfoResp{}, err
	}
	res := &ss.GetSalonAllInfoResp{
		Id: responce.Id,
		CustomerId: responce.CustomerId,
		AutoId: responce.AutoId,
		SalonName: responce.SalonName,
		Description: responce.Description,
	}
	for _, res_in := range responce.Locations{
		temp := &ss.Location{
			LocationId: res_in.LocationId,
			SalonId: res.Id,
			Country: res_in.Country,
			Street: res_in.Street,
			District: res_in.District,
			PhoneNumber: res_in.PhoneNumber,
		}
		res.Locations = append(res.Locations, temp)
	}
	auto, err := s.Client.AutoInfo().GetAuto(ctx, &autoinfo.GetAutoReq{AutoId: res.AutoId})
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting AUTO INFO`, logger.New(err.Error()))
		return nil, err
	}
	auto_info := &ss.GetAutoResp{
		Id: auto.Id,
		AutoName: auto.AutoName,
		Model: auto.Model,
		SalonId: auto.SalonId,
		CustomerId: auto.CustomerId,
		Price: auto.Price,
		Country: auto.Country,
	}
	for _, auto_in := range auto.Types {
		plus := &ss.AutoType{
			Id: auto_in.Id,
			AutoId: auto_info.Id,
			Auto_Type: auto_in.Auto_Type,
			Marka: auto_in.Marka,
		}
		auto_info.Types = append(auto_info.Types, plus)
	}
	res.GetAuto = append(res.GetAuto, (*ss.GetAutoResp)(auto_info))

	customer, err := s.Client.Customer().GetCustomers(ctx, &cc.CustomerID{CustomerId: responce.CustomerId})
	if err != nil {
		fmt.Println(err)
		s.Logger.Error(`Error while getting Customer`, logger.New(err.Error()))
		return nil, err
	}
	cus := &ss.GetCustomerResp{
		Id: res.CustomerId,
		SalonId: customer.SalonId,
		AutoId: customer.AutoId,
		Fullname: customer.Fullname,
		Email: customer.Email,
		Phonenumber: customer.Phonenumber,
	}
	for _, address := range customer.Addresses {
		fmt.Println(`Hello`)
		temp := &ss.Address{
			Id: address.Id,
			CustomerId: cus.Id,
			District: address.District,
			Street: address.Street,
			Homenumber: address.Homenumber,
		}
		cus.Addresses = append(cus.Addresses, temp)
	}
	fmt.Println(cus.Addresses)
	res.GetCustomer = append(res.GetCustomer, (*ss.GetCustomerResp)(cus))
	
return res, nil
}


// for _, cus := range customer.Addresses{
// 	temp := &ss.Address{
// 		Id: cus.Id,
// 		CustomerId: res.CustomerId,
// 		District: cus.District,
// 		Street: cus.Street,
// 		Homenumber: cus.Homenumber,
// 	}
// 	customer.Addresses = append(customer.Addresses, (*cc.Address)(temp))
// }
// res.GetCustomer = append(res.GetCustomer, responce.GetCustomer...)


// func (s *SalonService) GetSalonWithCustomer(ctx context.Context, req *ss.GetSalonReq) (*ss.GetSalonWithCustomerResp, error) {	

// 	responce, err := s.Storage.Salon().GetSalon(req)
// 	if err != nil {
// 		s.Logger.Error(`Error while getting SALON`, logger.New(`Error with SALON`))
// 		return nil, err
// 	}
// 	res := &ss.GetSalonWithCustomerResp{
// 		Id: responce.Id,
// 		SalonName: responce.SalonName,
// 		Description: responce.Description,
// 	}
// 	for _, res_in := range responce.Locations {
// 		temp := &ss.Location{
// 			LocationId: res_in.LocationId,
// 			SalonId: res.Id,
// 			Country: res_in.Country,
// 			Street: res_in.Street,
// 			District: res_in.District,
// 			PhoneNumber: res_in.PhoneNumber,
// 		}
// 		res.Locations = append(res.Locations, temp)
// 	}
	
// 	customer, err := s.Client.Customer().GetCustomers(ctx, &cc.CustomerID{CustomerId: responce.Id})
// 	if err != nil {
// 		s.Logger.Error(`Error while getting CUSTOMER`, logger.New(`Error with CUSTOMER`))
// 		return nil, err
// 	}
// 	cus := &ss.CustomerInfo{
// 		Id: customer.Id,
// 		Fullname: customer.Fullname,
// 		Email: customer.Email,
// 		Phonenumber: customer.Phonenumber,
// 		SalonId: responce.Id,
// 	}
// 	for _, cus_in := range cus.Addresses {
		
// 		address := &ss.Address{
// 			Id: cus_in.Id,
// 			CustomerId: cus.Id,
// 			District: cus_in.District,
// 			Street: cus_in.Street,
// 			Homenumber: cus_in.Homenumber,
// 		}
// 		cus.Addresses = append(cus.Addresses, (*ss.Address)(address))
// 	}
// 	responce.GetCustomer = append(responce.GetCustomer, cus)

// return (*ss.GetSalonWithCustomerResp)(responce), nil
// }