package grpcclient

import (
	"fmt"

	"gitlab.com/salon5/salon_service/config"
	aa "gitlab.com/salon5/salon_service/internal/genproto/autoinfo"
	cc "gitlab.com/salon5/salon_service/internal/genproto/customer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Clients interface {
	AutoInfo() aa.AutoInfoServiceClient
	Customer() cc.CustomerServiceClient
}

type ServiceManager struct {
	Config config.Config
	autoInfoService aa.AutoInfoServiceClient
	customerService cc.CustomerServiceClient
}

func New(cfg config.Config) (*ServiceManager, error) {
	
	connAutoInfo, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.AutoInfoServiceHost, cfg.AutoInfoServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}

	connCustomer, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.CustomerServiceHost, cfg.CustomerServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}

	return &ServiceManager{
		Config: cfg,
		autoInfoService: aa.NewAutoInfoServiceClient(connAutoInfo),
		customerService: cc.NewCustomerServiceClient(connCustomer),
	},nil
}

func (s *ServiceManager) AutoInfo() aa.AutoInfoServiceClient {
	return s.autoInfoService
}

func (s *ServiceManager) Customer() cc.CustomerServiceClient {
	return s.customerService
}
