package models

type SendSMSVerificationCodeReq struct {
	To   string `json:"to"`
	Code string `json:"code"`
}
