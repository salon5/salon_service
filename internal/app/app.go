package app

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/salon5/salon_service/config"
	service "gitlab.com/salon5/salon_service/internal/controller/service"
	grpcclient "gitlab.com/salon5/salon_service/internal/controller/service/grpcClient"
	"gitlab.com/salon5/salon_service/internal/genproto/salon"
	messagebroker "gitlab.com/salon5/salon_service/internal/message_broker"
	"gitlab.com/salon5/salon_service/pkg/db"
	"gitlab.com/salon5/salon_service/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func Run(cfg *config.Config) {
	l := logger.New(cfg.LogLevel)

	pgxUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
	cfg.PostgresUser,
	cfg.PostgresPassword,
	cfg.PostgresHost,
	cfg.PostgresPort,
	cfg.PostgresDatabase,
	)

	pg, err := db.New(pgxUrl, db.MaxPoolSize(cfg.PGXPOOLMAX))
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - postgres.New: %w", err))
	}
defer pg.Close()
	
	clients, err := grpcclient.New(*cfg)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}
	kafka, err := messagebroker.NewProducer(*cfg)
	if err != nil {
		l.Fatal(fmt.Errorf("Error while connecting to kafka: %w", err))
	}

	salonService := service.NewSalonService(l, clients, pg, kafka)

	lis, err := net.Listen("tcp", ":" + cfg.SalonServicePort)
	if err != nil {
		fmt.Println(err)
		l.Fatal(fmt.Errorf("app - Run - grpcClient.New: %w", err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	salon.RegisterSalonServiceServer(c, salonService)
	
	l.Info("Server is running on" + "port" + ":" + cfg.SalonServicePort)

	if err :=c.Serve(lis); err != nil {
		log.Fatal("Error while listening: ", err)
	}

}	